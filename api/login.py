# 导包
import requests

"""
被测系统的接口封装
登录: "http://ihrm2-test.itheima.net/api/sys/login"
"""


# 定义接口类
class LoginAPI:
    # 初始化
    def __init__(self):
        self.login_url = "http://ihrm2-test.itheima.net/api/sys/login"

    # 登录接口
    def login(self, login_data):
        # 这里不是表单而是json
        return requests.post(url=self.login_url, json=login_data)


if __name__ == '__main__':
    response = LoginAPI().login({"mobile": "13800000002", "password": "123456"})
    print(response.json())
