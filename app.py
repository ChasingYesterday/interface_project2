"""
这个文件夹放的是配置信息
"""
import os

# 项目基础URL -- 测试环境
BASE_URL = "http://ihrm2-test.itheima.net"

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

# token
TOKEN = None

# 请求头数据
# happy!!!!!!!!!我发现每一个请求都需要这个header_data, 这也是headers_data放在这里的原因
headers_data = {
    "Content-Type": "application/json",
    "Authorization": "Bearer bcfc9d2e-a575-4240-9569-f5f860619f97"
}